import random

dice = [4, 6, 8, 12, 20]
priors = {die: 0.20 for die in dice}

def choose_die(dice):
    return random.choice(dice)

def roll_die(die):
    return random.randint(1, die)

def update_priors(dice, priors, roll):
    numerators = []
    for die in dice:
        likelihood = lambda: 0 if roll > die else (1 / die)
        prior = priors[die]
        numerator = likelihood() * prior
        numerators.append(numerator)
    normalizer = sum(numerators)
    return {d: numerators[i] / normalizer for i, d in enumerate(dice)}

d = choose_die(dice)
for _ in range(10):
    roll = roll_die(d)
    priors = update_priors(dice, priors, roll)
    print('die:', d, 'roll:', roll, 'priors:', priors)

guess = max(priors, key=lambda k: priors[k])
print('actual:', d, 'guess:', guess)

