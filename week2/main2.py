class Node:
    def __init__(self, value):
        self.value = value
        self.left = None
        self.right = None

class Tree:
    def __init__(self):
        self.root = None
    def append(self, word):
        if not self.root:
            self.root = Node(word)
        else:
            self._append(self.root, word)

    def _append(self, parent, word):
        if word < parent.value:
            if not parent.left:
                parent.left = Node(word)
            else:
                self._append(parent.left, word)
        else:
            if not parent.right:
                parent.right = Node(word)
            else:
                self._append(parent.right, word)

words = 'doug charlie barbie alexander zack yellow chuck'.split(' ')
t = Tree()
for word in words:
    t.append(word)
