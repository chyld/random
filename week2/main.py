class Node:
    def __init__(self, value):
        self.value = value
        self.next = None

class Chain:
    def __init__(self):
        self.head = None
    def attach1(self, node):
        if not self.head:
            self.head = node
        else:
            node.next = self.head
            self.head = node
    def attach2(self, node):
        if not self.head:
            self.head = node
        else:
            n = self.head
            if node.value < n.value:
                node.next = n


c = Chain()
c.attach1(Node(3))
c.attach1(Node(5))
c.attach1(Node(7))
